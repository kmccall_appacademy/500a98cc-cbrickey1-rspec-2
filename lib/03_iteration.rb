# require 'byebug'


def factors(num)
  factor_array = [1]
  (2..num).each { |divisor| factor_array << divisor if num % divisor == 0}
  factor_array
end


def subwords(str, arr)
  arr_of_matches = Array.new
  arr.each { |word| arr_of_matches << word if str.include?(word) }
  arr_of_matches
end


def doubler(arr)
  arr.map { |n| n * 2 }
end



class Array

  def bubble_sort!(&prc)
    return self if self == [] || self.length == 1
    prc ||= Proc.new { |num1, num2| num2 <=> num1 }
    self.map! { |num1, num2| prc.call(num1, num2) }
  end


  def bubble_sort
    new_array = self.dup
    new_array.bubble_sort!
  end


  def my_each(&prc)
    0.upto(self.length - 1) do |idx|
      prc.call(self[idx])
    end
    self
  end


  def my_map(&prc)
    transformed_array = Array.new
    self.my_each { |element| transformed_array << prc.call(element) }
    transformed_array
  end


  def my_select(&prc)
    selected_array = Array.new
    self.my_each { |element| selected_array << element if prc.call(element) }
    selected_array
  end


  def my_inject(&blk)
    accumulator = self.shift
    self.my_each { |element| accumulator = blk.call(accumulator, element) }
    accumulator
  end

end#of class


def concatenate(strings)
  final_string = String.new
  strings.inject(final_string) { |final_string, word| final_string << "#{word}" }
  final_string
end
