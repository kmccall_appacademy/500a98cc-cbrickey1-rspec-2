def reverser
  word_array = yield.split(/\s+/)
  word_array.map! { |word| word.reverse }
  word_array.join(" ")
end


def adder(addend = 1)
  yield + addend
end


def repeater(n = 1)
  n.times { yield }
end
